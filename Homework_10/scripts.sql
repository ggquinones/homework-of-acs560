.header on
.mode column

create table CourseEvaluation(CourseID int primary key not null, ProfessorID int, Evaluation double);
insert into CourseEvaluation values(1, 1, 4.0);
insert into CourseEvaluation values(2, 2, 3.5);
insert into CourseEvaluation values(3, 3, 4.0);
insert into CourseEvaluation values(4, 4, 3.3);
insert into CourseEvaluation values(5, 5, 3.0);
insert into CourseEvaluation values(6, 6, 2.0);
insert into CourseEvaluation values(7, 7, 2.8);
insert into CourseEvaluation values(8, 8, 3.5);
insert into CourseEvaluation values(9, 9, 3.9);
insert into CourseEvaluation values(10, 10, 4.0);
insert into CourseEvaluation values(11, 10, 3.9);
insert into CourseEvaluation values(12, 10, 3.8);
insert into CourseEvaluation values(13, 11, 4.0);
insert into CourseEvaluation values(14, 8, 2.1);

select * from CourseEvaluation;


-- SQL #1 Gets average per teacher first in a table and
-- then gets average of those teacherAvgs defined in the inner select
select avg(teacherAvg) Evaluation_Average
from
(
    select professorid, avg(evaluation) teacherAvg from courseevaluation group by professorid
);

-- SQL #2 Here we take again the inner table from sql 1 above and simply get those
-- teachers whose avgs are above average using the having statement


select professorid teacher_ID, avg(evaluation) Evaluation_Avg
from CourseEvaluation
group by professorid
having Evaluation_Avg >
(
    select avg(profeva) aveeva
    from
    (
        select professorid, avg(evaluation) profeva
        from courseevaluation
        group by professorid
    )
);