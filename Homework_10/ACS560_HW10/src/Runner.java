import java.sql.*;

public class Runner 
{

	public static void main(String[] args) throws SQLException 
	{
		Connection conn = DriverManager.getConnection("jdbc:sqlite:sample.db");
		System.out.println("Connection to SQLite has been established.");
		String tableSQL = "create table courses (semester varchar(20),course varchar(35));";
		Statement stmt = conn.createStatement();
		stmt.execute(tableSQL);
		System.out.println("Table Created");
		
		String insertSQL ="insert into courses values('Fall 2016','Software Engineering');";
		System.out.println("ADDED ROW: ('Fall 2016','Software Engineering')");
		String insertSQL2="insert into courses values('Spring 2016','Software Process Mgmt.');";
		System.out.println("ADDED ROW: ('Spring 2016','Software Process Mgmt.')");
		stmt.execute(insertSQL);
		stmt.execute(insertSQL2);
		
		String selectSql = "select * from courses;";
		ResultSet rs = stmt.executeQuery(selectSql);
		
		while(rs.next()) 
		{
			System.out.println(rs.getString("semester")+" "+ rs.getString("course"));
		}
		
        
	}
	
	

}
