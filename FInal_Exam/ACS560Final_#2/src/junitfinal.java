import static org.junit.Assert.*;

import org.junit.Test;

public class junitfinal {

	
	@Test(expected = RuntimeException.class)
	public void divideByZero() {
		int answer = Calculator.CalculatorFromReversePolish("10 0 /");
		
	}
	@Test 
	   public void subtraction() {
			int answer = Calculator.CalculatorFromReversePolish("100 50 -");
			
	      assertTrue(answer==50);
	   }

	@Test 
	   public void addition() {
			int answer = Calculator.CalculatorFromReversePolish("100 50 +");
			
	      assertTrue(answer==150);
	   }
	
	@Test
	   public void multiplication() {
			int answer = Calculator.CalculatorFromReversePolish("100 50 *");
	
	      assertTrue(answer==5000);
	   }
	@Test
	   public void division() {
			int answer = Calculator.CalculatorFromReversePolish("10 2 /");
			
	      assertTrue(answer==5);
	   }
	@Test(expected = EmptyStackException.class)
	public void incorrectNumberOfOperands() {
		int answer = Calculator.CalculatorFromReversePolish("-50 -");
	}
	
	@Test(expected = EmptyStackException.class)
	public void incorrectInput() {
		int answer = Calculator.CalculatorFromReversePolish("1 - 1");
	}
	
	@Test
	public void exampleFromTest() {
		int answer = Calculator.CalculatorFromReversePolish("1 2 - 4 5 + *");
		assertTrue(answer==-9);
	}
	
	
}
