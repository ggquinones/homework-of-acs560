import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Arrays;

public class TestJUnit {
   
	@Test 
   public void testInputLessThan2NewCodeTest() {
		int [] nums = PrimeGenerator.generatePrimes(1);
      assertTrue(nums.length ==1 && nums[0]==-1 );
   }
	
	@Test // tests that input of 3 returns correct output
		// first input deemed as valid by system
	   public void testInput3Correct() {
			int [] nums = PrimeGenerator.generatePrimes(3);
			int [] expected = {2,3};
	      assertTrue(Arrays.equals(nums, expected ));
	   }
	
	@Test // tests that input of 30 returns correct output
   public void testInput30Correct() {
		int [] nums = PrimeGenerator.generatePrimes(30);
		
		int [] expected = {2,3,5,7,11,13,17,19,23,29};
      assertTrue(Arrays.equals(nums, expected ));
   }
	
	@Test // tests that input of 100 returns correct output
	   public void testInput100Correct() {
			int [] nums = PrimeGenerator.generatePrimes(100);
			
			int [] expected = {2,3,5,7,11,13,17,19,23,29,
					31,37,41,43,47,53,59,61,67,71,73,79,83,89,97};
	      assertTrue(Arrays.equals(nums, expected ));
	   }
	
}