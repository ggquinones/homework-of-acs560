/*
    Gabriel Quinones-Sanchez
    This program is meant to run Newton's Method for approximating the square root
    of a given number.
*/
package main

import (
	"fmt"
	"math"
)

// Iterates simply ten times as stated in the problem.
func NewtonsMethod1(x float64) float64 {
	z:= 1.0
	for n:=0;n<10;n++ {
		z = z - (z*z-x)/(z*2)
	}
	return z
}
// Essentially an infinite loop that runs until the difference in progressive z values is < 0.00000000001
func NewtonsMethod2(x float64) (float64,int){
    z:=1.0
    iterations :=0
    for  {
    	if math.Abs(z-(z-(z*z-x)/(z*2))) < 0.00000000001 {
    	    return z,iterations
    	} else {
    		z = z - (z*z-x)/(z*2)
    		iterations+=1
    	}
	}
}

func main() {
    // Used to run test cases for running the method 10 times. Compared to actual result
    fmt.Println("Using 10 iterations with value of:",10.0)
    fmt.Println("Actual:", math.Sqrt(10.0))
    fmt.Println("Newtons Method 1:",NewtonsMethod1(10.0))
    fmt.Println("Abs. Diff.:", math.Abs(math.Sqrt(10.0) - NewtonsMethod1(10.0) ))

    // Even running the method with 1000 the absolute difference continues to be 0. So the method is very accurate, though the formatting takes away digits
    // 6 decimal places where there will probably be a difference between my function and the math.Sqrt method

    fmt.Println("Using delta as loop control with value of:",10.0)
    fmt.Println("Actual:", math.Sqrt(10.0))
    n,i := NewtonsMethod2(10.0)
    fmt.Println("Newtons Method 2:",n)
    fmt.Println("Iterations",i)
    fmt.Println("Abs. Diff.:", math.Abs(math.Sqrt(10.0) - n ))

    //When you run this it takes less iterations if you loop based on the delta instead of a set number of times!


}