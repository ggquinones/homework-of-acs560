﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SimpleCalculator
{
    public partial class Form1 : Form
    {
        double operand1, operand2, result;

        private void btnSubtract_Click(object sender, EventArgs e)
        {
            operand1 = Convert.ToDouble(txtBoxFirstNumber.Text);
            operand2 = Convert.ToDouble(txtBoxSecondNumber.Text);
            result = operand1 - operand2;
            txtBoxResult.Text = result.ToString();
        }

        private void btnMultiply_Click(object sender, EventArgs e)
        {
            operand1 = Convert.ToDouble(txtBoxFirstNumber.Text);
            operand2 = Convert.ToDouble(txtBoxSecondNumber.Text);
            result = operand1 * operand2;
            txtBoxResult.Text = result.ToString();
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            if (Convert.ToDouble(txtBoxSecondNumber.Text) != 0)
            {
                operand1 = Convert.ToDouble(txtBoxFirstNumber.Text);
                operand2 = Convert.ToDouble(txtBoxSecondNumber.Text);
                result = operand1 / operand2;
                txtBoxResult.Text = result.ToString();
            }
            else
            {

            }
            
        }

        private void txtBoxFirstNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == false && char.IsControl(e.KeyChar) == false && (e.KeyChar == '.' ? txtBoxFirstNumber.Text.Contains('.') == true : true))
            {
                e.Handled = true;   
            }
        }

        private void txtBoxSecondNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == false && char.IsControl(e.KeyChar) == false && (e.KeyChar == '.' ? txtBoxSecondNumber.Text.Contains('.') == true : true))
            {
                e.Handled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            operand1 = Convert.ToDouble(txtBoxFirstNumber.Text);
            operand2 = Convert.ToDouble(txtBoxSecondNumber.Text);
            result = operand1 + operand2;
            txtBoxResult.Text = result.ToString();
        }

        public Form1()
        {
            InitializeComponent();
        }

        
    }
}
