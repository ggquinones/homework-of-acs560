﻿namespace SimpleCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelFirstNum = new System.Windows.Forms.Label();
            this.txtBoxFirstNumber = new System.Windows.Forms.TextBox();
            this.labelSecondNumber = new System.Windows.Forms.Label();
            this.txtBoxSecondNumber = new System.Windows.Forms.TextBox();
            this.txtBoxResult = new System.Windows.Forms.TextBox();
            this.labelResult = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDivide = new System.Windows.Forms.Button();
            this.btnMultiply = new System.Windows.Forms.Button();
            this.btnSubtract = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelFirstNum
            // 
            this.labelFirstNum.AutoSize = true;
            this.labelFirstNum.Location = new System.Drawing.Point(23, 32);
            this.labelFirstNum.Name = "labelFirstNum";
            this.labelFirstNum.Size = new System.Drawing.Size(73, 13);
            this.labelFirstNum.TabIndex = 0;
            this.labelFirstNum.Text = "First Operand:";
            // 
            // txtBoxFirstNumber
            // 
            this.txtBoxFirstNumber.Location = new System.Drawing.Point(124, 31);
            this.txtBoxFirstNumber.Name = "txtBoxFirstNumber";
            this.txtBoxFirstNumber.Size = new System.Drawing.Size(75, 20);
            this.txtBoxFirstNumber.TabIndex = 1;
            this.txtBoxFirstNumber.Text = "0.00";
            this.txtBoxFirstNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxFirstNumber_KeyPress);
            // 
            // labelSecondNumber
            // 
            this.labelSecondNumber.AutoSize = true;
            this.labelSecondNumber.Location = new System.Drawing.Point(12, 75);
            this.labelSecondNumber.Name = "labelSecondNumber";
            this.labelSecondNumber.Size = new System.Drawing.Size(91, 13);
            this.labelSecondNumber.TabIndex = 2;
            this.labelSecondNumber.Text = "Second Operand:";
            // 
            // txtBoxSecondNumber
            // 
            this.txtBoxSecondNumber.Location = new System.Drawing.Point(124, 72);
            this.txtBoxSecondNumber.Name = "txtBoxSecondNumber";
            this.txtBoxSecondNumber.Size = new System.Drawing.Size(72, 20);
            this.txtBoxSecondNumber.TabIndex = 3;
            this.txtBoxSecondNumber.Text = "0.00";
            this.txtBoxSecondNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxSecondNumber_KeyPress);
            // 
            // txtBoxResult
            // 
            this.txtBoxResult.Location = new System.Drawing.Point(72, 138);
            this.txtBoxResult.Name = "txtBoxResult";
            this.txtBoxResult.ReadOnly = true;
            this.txtBoxResult.Size = new System.Drawing.Size(112, 20);
            this.txtBoxResult.TabIndex = 4;
            this.txtBoxResult.Text = "0.00";
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(104, 119);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(37, 13);
            this.labelResult.TabIndex = 5;
            this.labelResult.Text = "Result";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(26, 183);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(46, 23);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "+";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDivide
            // 
            this.btnDivide.Location = new System.Drawing.Point(182, 183);
            this.btnDivide.Name = "btnDivide";
            this.btnDivide.Size = new System.Drawing.Size(46, 23);
            this.btnDivide.TabIndex = 7;
            this.btnDivide.Text = "/";
            this.btnDivide.UseVisualStyleBackColor = true;
            this.btnDivide.Click += new System.EventHandler(this.btnDivide_Click);
            // 
            // btnMultiply
            // 
            this.btnMultiply.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMultiply.Location = new System.Drawing.Point(130, 183);
            this.btnMultiply.Name = "btnMultiply";
            this.btnMultiply.Size = new System.Drawing.Size(46, 23);
            this.btnMultiply.TabIndex = 8;
            this.btnMultiply.Text = "*";
            this.btnMultiply.UseVisualStyleBackColor = true;
            this.btnMultiply.Click += new System.EventHandler(this.btnMultiply_Click);
            // 
            // btnSubtract
            // 
            this.btnSubtract.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubtract.Location = new System.Drawing.Point(78, 183);
            this.btnSubtract.Name = "btnSubtract";
            this.btnSubtract.Size = new System.Drawing.Size(46, 23);
            this.btnSubtract.TabIndex = 9;
            this.btnSubtract.Text = "-";
            this.btnSubtract.UseVisualStyleBackColor = true;
            this.btnSubtract.Click += new System.EventHandler(this.btnSubtract_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnSubtract);
            this.Controls.Add(this.btnMultiply);
            this.Controls.Add(this.btnDivide);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.txtBoxResult);
            this.Controls.Add(this.txtBoxSecondNumber);
            this.Controls.Add(this.labelSecondNumber);
            this.Controls.Add(this.txtBoxFirstNumber);
            this.Controls.Add(this.labelFirstNum);
            this.Name = "Form1";
            this.Text = "Simple Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFirstNum;
        private System.Windows.Forms.TextBox txtBoxFirstNumber;
        private System.Windows.Forms.Label labelSecondNumber;
        private System.Windows.Forms.TextBox txtBoxSecondNumber;
        private System.Windows.Forms.TextBox txtBoxResult;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDivide;
        private System.Windows.Forms.Button btnMultiply;
        private System.Windows.Forms.Button btnSubtract;
    }
}

