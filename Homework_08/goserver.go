package main

import (
    "fmt"
    "net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hello ACS560!")
}

func main() {
    http.HandleFunc("/", handler)
    http.ListenAndServe(":12000", nil)
}
