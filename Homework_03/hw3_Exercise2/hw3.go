package main

import (
	"fmt"
	"io/ioutil"
)

// Simple error handling to avoid crashing the program without warning
func check(e error) {
	if e != nil {
		panic(e)
	}
}

func makeTableRow(col1 string, col2 string) string {
	return ("<tr><td>" + col1 + "</td><td>" + col2 + "</td></tr>")
}

// Makes a file "hw3_copy.html" and copies contents of original HTML file into it
func copyFile(content string) {
	data := []byte(content)
	err2 := ioutil.WriteFile("hw3_copy.html", data, 0644)
	check(err2)
	fmt.Println("File generated succesfully!")
}

// Main function
func main() {
	content := "<!DOCTYPE>" +
		"<html>" +
		"<head>" +
		"<title>ACS 560 HW_3</title>" +
		"<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">" +
		"<style>" +
		"table {" +
		"width:100%;" +
		"}" +
		"table,td{" +
		"border-style:solid;" +
		"}" +
		"</style>" +
		"</head>" +
		"<body>" +
		"<div id=\"content\" class=\"w3-row\">" +
		"<div id=\"leftSpacerCol\" class=\"w3-col s2\"><p><!--Spacer Column --></p></div>" +
		"<div id=\"middleDisplayCol\" class=\"w3-col s8\">" +
		"<table>"

	column1 := [4]string{"Course", "ACS560", "ACS577", "ACS590"}
	column2 := [4]string{"Semester", "Fall 2017", "Fall 2017", "Fall 2017"}
	for i := 0; i < len(column1); i++ {
		content += makeTableRow(column1[i], column2[i])
	}

	content += "</table>" +
		"</div>" +
		"<div id=\"rightSpacerCol\" class=\"w3-col s2\"><p><!--Spacer Column --></p></div>" +
		"</div>" +
		"</body>" +
		"</html>"

	copyFile(content)
}
