﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ACS550_HW3
{
    class Program
    {
        public static void generateFile(string content)
        {
            File.WriteAllText(@"hw3_copy.html",content);
            Console.WriteLine("File generated!");
            Console.ReadLine();
        }
        public static string addTableRow(string col1,string col2)
        {
            return ("<tr><td>" + col1 + "</td><td>" + col2 + "</td></tr>");
        }

        static void Main(string[] args)
        {
            string content = "<!DOCTYPE>" +
            "<html>" +
            "<head>" +
            "<title>ACS 560 HW_3</title>" +
            "<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">" +
            "<style>" +
            "table {" +
            "width:100%;" +
            "}" +
            "table,td{" +
            "border-style:solid;" +
            "}" +
            "</style>" +
            "</head>" +
            "<body>" +
            "<div id=\"content\" class=\"w3-row\">" +
            "<div id=\"leftSpacerCol\" class=\"w3-col s2\"><p><!--Spacer Column --></p></div>" +
            "<div id=\"middleDisplayCol\" class=\"w3-col s8\">" +
            "<table>";
            string[] column1 = {"Course","ACS560","ACS577","ACS590"};
            string[] column2 = {"Semester","Fall 2017", "Fall 2017", "Fall 2017" };

            for (int i=0;i< column1.Length;i++)
            {
                content += addTableRow(column1[i],column2[i]);
            }
            content += "</table>" +
                        "</div>" +
                        "<div id=\"rightSpacerCol\" class=\"w3-col s2\"><p><!--Spacer Column --></p></div>" +
                        "</div>" +
                        "</body>" +
                        "</html>";

            generateFile(content);
        }
    }
}
