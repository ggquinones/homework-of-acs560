import java.util.Scanner;

public class Game 
{
	Scorer scorer = new Scorer();
	
	
	
	public void add(int pins)
	{
		scorer.add(pins);
	}
	
	public int score()
	{
		return scorer.score();
	}
	
	public Scorer getScorer()
	{
		return scorer;
	}
}
	