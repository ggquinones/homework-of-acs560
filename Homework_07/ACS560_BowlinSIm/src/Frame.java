import java.util.ArrayList;
public class Frame 
{
	boolean strike,spare;
	ArrayList<Integer> frameThrows;
	int frameTotal;
	
	public Frame()
	{
		this.strike =false;
		this.spare = false;
		frameThrows = new ArrayList<Integer>();
		frameTotal = 0;
				
	}
	
	public void add(int pins)
	{
		frameThrows.add(pins);
	}
	
	public boolean isStrike()
	{
		return strike;
	}
	
	public boolean isSpare()
	{
		return spare;
	}
	
	
	public int getFrameTotal()
	{
		int sum = 0;
		for(int _throw: frameThrows)
		{
			sum += _throw;
		}
		return sum;
	}
	
	public boolean makesSpare(int pins)
	{
		return pins + getFrameTotal() == 10;
	}
	
	public void setStrike()
	{
		this.add(10);
		this.strike = true;
	}
	
	
	
	public void setSpare(int pins)
	{
		frameThrows.add(pins);
		this.spare = true;
	}
	
	public void addToTotal(int pins)
	{
		if(frameThrows.size()==1)
		{
			this.add(pins);
		}
		else {
			int firstExtraThrow = frameThrows.get(1);
			frameThrows.set(1, firstExtraThrow +pins);
		}
	}

	public int getFirstThrow() 
	{
		int answer = frameThrows.get(0);
		return answer;
	}
	
	public int getSecondThrow()
	{
		int answer = frameThrows.get(1);
		return answer;
	}
	
	
}
