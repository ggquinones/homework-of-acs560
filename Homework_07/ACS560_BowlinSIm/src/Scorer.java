import java.util.ArrayList;
public class Scorer 
{
	private ArrayList<Frame> allFrames; 
	private int totalScore;
	 private int currFrame = 0;
	private int currThrow = 1;
	public Scorer()
	{
		allFrames = new ArrayList<Frame>();
		
	}
	
	public boolean keepPlaying()
	{
		if(currFrame>= 0 && currFrame<=9) // 0-9
		{
			return true;
		}
		else if( currFrame == 10 && (allFrames.get(9).isSpare() || allFrames.get(9).isStrike()) ) // Last Bowl is Spare & currFrame==10
		{
			return true;
		}
		
		
		
		
		return false;
	}
	
	public int score()
	{
		int total =0;
		for(int i=0;i<allFrames.size();i++)
		{
			if(i == 9)
			{
				total+= allFrames.get(9).getFrameTotal();
			}
			else if(!allFrames.get(i).isSpare() && !allFrames.get(i).isStrike())
			{
				total += allFrames.get(i).getFrameTotal();
			}
			else if(allFrames.get(i).isSpare())
			{
				total += allFrames.get(i).getFrameTotal();
				try {
					total += allFrames.get(i+1).getFirstThrow();
				}catch(Exception e) {}
			}
			else if(allFrames.get(i).isStrike())
			{
				total+=allFrames.get(i).getFrameTotal();
				try
				{
					
					if(allFrames.get(i+1).isStrike())
					{
						if(i==8)
						{
							total+= allFrames.get(i+1).getFirstThrow();
							total+= allFrames.get(i+1).getSecondThrow()-10;
						}
						else
						{
							total+= allFrames.get(i+1).getFirstThrow();
							total+=allFrames.get(i+2).getFirstThrow();
						}
						
					}
					else
					{
						total+= allFrames.get(i+1).getFrameTotal();
					}
					
					
				}catch(Exception e) {}
			}
		}
		
		
		
		return total;
	}
	
	public void add(int pins)
	{
		if(currFrame<=9)
		{
			if(currThrow == 1)
			{
				allFrames.add(new Frame());
				if(pins==10)
				{
					allFrames.get(currFrame).setStrike();
					//System.out.println("Frame # :"+currFrame+ ",Score: "+allFrames.get(currFrame).getFrameTotal());
					currFrame++;
					currThrow=1;
				}
				else
				{
					allFrames.get(currFrame).add(pins);
					currThrow++;
				}
			}
			else
			{
				if(allFrames.get(currFrame).makesSpare(pins))
				{
					allFrames.get(currFrame).setSpare(pins);
					//System.out.println("Frame # :"+currFrame+ ",Score: "+allFrames.get(currFrame).getFrameTotal());
					currFrame++;
					currThrow=1;
				}
				else
				{
					allFrames.get(currFrame).add(pins);
					//System.out.println("Frame # :"+currFrame+ ",Score: "+allFrames.get(currFrame).getFrameTotal());
					currFrame++;
					currThrow=1;
				}
			}
		}
		else if(currFrame ==10)
		{
			if(allFrames.get(9).isSpare())
			{
				allFrames.get(9).addToTotal(pins);
				//System.out.println("Frame # :"+(currFrame) + ",Score: "+allFrames.get(9).getFrameTotal());
				currFrame++;
			}
			else if(allFrames.get(9).isStrike() && currThrow==1)
			{
				allFrames.get(9).addToTotal(pins);
				currThrow++;			
				//System.out.println("1st throw, Frame # :"+(currFrame)+ ",Score: "+allFrames.get(9).getFrameTotal());
						
			}
			else if(allFrames.get(9).isStrike() && currThrow==2)
			{
				allFrames.get(9).addToTotal(pins);
				currFrame++;			
				//System.out.println("1st throw, Frame # :"+(currFrame)+ ",Score: "+allFrames.get(9).getFrameTotal());
				
			}
		}
	}
	
	
}


