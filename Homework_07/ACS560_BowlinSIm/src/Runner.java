import java.util.Scanner;

public class Runner 
{

	public static void main(String[] args) 
	{
		
		// To test a case simply uncomment the test below:
		// (Test Descriptions by method headers)
		
		PerfectGameTest();
		//BestGameWithSpares();
		//GutterBalls();
		//WorstGameWithSpares();
		//AllFives();
	}

	// Perfect Game == 300 points (12 strikes in a row)
	public static void PerfectGameTest()
	{
		Game game = new Game();
		int score =10;
		while(game.getScorer().keepPlaying())
		{
			
			game.add(score);
			
		}
		System.out.println(game.getScorer().score());
	}
	// If you roll a 9-1 spare every time and a 9 on your extra roll your score would be 190
	public static void BestGameWithSpares()
	{
		Game game = new Game();
		
		int score = 9;
		while(game.getScorer().keepPlaying())
		{
			game.add(score);
			
			if(score ==9)
			{
				score=1;
			}
			else
			{
				score =9;
			}
		}
		System.out.println(game.getScorer().score());
	}
	// terrrible day at the lanes and you bowl all zeros
	public static void GutterBalls()
	{
		Game game = new Game();
		
		int score = 0;
		while(game.getScorer().keepPlaying())
		{
			game.add(score);
			
			
		}
		System.out.println(game.getScorer().score());
	}
	
	// shabby day at the lanes and you bowl all 5-5 spares - 150 points
		public static void AllFives()
		{
			Game game = new Game();
			
			int score = 5;
			while(game.getScorer().keepPlaying())
			{
				game.add(score);
				
				
			}
			System.out.println(game.getScorer().score());
		}
	// If you rolled a 0-10 spare every roll and a 0 on your last roll your score would be 100
	public static void WorstGameWithSpares()
	{
		Game game = new Game();
		
		int score = 0;
		while(game.getScorer().keepPlaying())
		{
			game.add(score);
			
			if(score ==0)
			{
				score=10;
			}
			else
			{
				score =0;
			}
		}
		System.out.println(game.getScorer().score());
	}
}
